import requests
from bs4 import BeautifulSoup 
import csv
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import matplotlib.pyplot as plt



url='https://www.imdb.com/search/title/?release_date=1999-01-01,2019-12-31&groups=oscar_winner'
pagina=requests.get(url)
if pagina.status_code !=200:
    print(f'Erro no download da página: {pagina.status_code}')
    exit()

soup=BeautifulSoup(pagina.text,'html.parser')
#print(soup.prettify())

filmes=soup.find_all('div',class_='lister-item mode-advanced')
#h3_descricao=filmes.find_parents('span',class_='text-muted').text

lista_filmes=[]
lista_filmes.append(['Nome','ano','rating','minutos','categoria','certificate','descricao','realizador','Votes','lucro'])

for f in filmes:
    print('\n')
    h3_nome=f.find('h3',class_='lister-item-header')
    nome_filme=h3_nome.find('a').text
    print(nome_filme)
    h3_ano=f.find('span',class_='lister-item-year text-muted unbold').text
    print(h3_ano)
    h3_rating=f.find('div',class_='inline-block ratings-imdb-rating').text.strip()
    print(h3_rating)
    h3_minutos=f.find('span',class_='runtime').text
    print(h3_minutos)
    h3_categoria=f.find('span',class_='genre').text.strip()
    print(h3_categoria)
    h3_certificate=f.find('span',class_='certificate').text
    print(h3_certificate)
    
    h3_Descricao=f.find_all('p',class_='text-muted')
    cont=0
    variavel_descricao=0
    for descr in h3_Descricao:
        
        a=descr.text.strip()      
        cont=cont+1
        if cont==2:

            variavel_descricao=a
    print(variavel_descricao)      
    
    realizador=[]
    h3_realizador=f.find('p',class_='').get_text()
    h3_realizador = h3_realizador.replace('\n', '')
    h3_realizador = h3_realizador.replace('|', '')
    h3_realizador = h3_realizador.replace(' ', '')
    h3_realizador = h3_realizador.replace('Stars', ',Stars')
    realizador.append(h3_realizador)
    def listToString(s):  
        str1 = ""   
        for ele in s:  
            str1 += ele    
        return str1  
    realizador=listToString(realizador)
    print(realizador)

    Votes=f.find('p',class_='sort-num_votes-visible').get_text().split()[1]
    print(Votes)
    Gross_val=0
    if len(f.find('p',class_='sort-num_votes-visible').get_text().split())==5:    
        Gross_val=f.find('p',class_='sort-num_votes-visible').get_text().split()[4]
    print(Gross_val)
    

    
    lista_filmes.append([nome_filme,h3_ano,h3_rating,h3_minutos,h3_categoria,h3_certificate,variavel_descricao,realizador,Votes,Gross_val])


with open('filmes.csv','w',newline="",encoding="UTF-8") as f:
    writer=csv.writer(f)
    writer.writerows(lista_filmes)

filmecsv=pd.read_csv('filmes.csv')

df_filmes=pd.DataFrame(filmecsv)
print('\n')
print(df_filmes.info())
print('\n')
print(df_filmes.describe())
print('\n')

ano_filmes=df_filmes['ano'].value_counts()
print(ano_filmes)

fig_pie = px.pie(df_filmes, values=ano_filmes.values, names=ano_filmes.index,title='Percentage of films that won Oscars per year')
fig_pie.show()


sort_locationuser=df_filmes.sort_values('rating', ascending=False)
df_barras=(sort_locationuser[0:3])
df_barras_nome=df_barras['Nome']
df_barras_rating=df_barras['rating']
fig, ax=plt.subplots()
ax.bar(df_barras_nome,df_barras_rating)
ax.set_ylabel('Rating dos Filmes')
ax.set_xlabel('Nome dos Filmes')
ax.set_title('Top 3 Rating Filmes')
plt.tight_layout()
plt.show()
