# Web Scraping - Python

_Explanation:_****

This program reads the existing information from a web page called IMDB. Some data taken are: the name of the film, the rating, the description, the directors etc. All information is stored in a CSV file and is processed through the Pandas library. It is possible to view statistical graphs of that file.

**Important**

Before running the .py you need to install:

    The following libraries:
        . requests
        . BeautifulSoup
        . numpy
        . pandas
        . matplotlib
        . plotly
